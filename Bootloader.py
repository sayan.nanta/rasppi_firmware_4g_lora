## pi@raspberrypi:~ $ crontab -e
## @reboot python3 rasppi_firmware_4g/Bootloader.py
## @reboot python3 rasppi_firmware_4g/BootWatchdog.py
## sudo pip3 install --upgrade requests
## For lora-e5

import LogService as LogService
import subprocess
import os
import time
import logging
import datetime
import _thread
# import Constants
# import MqttService as MqttService
# import SendData as SendData
import uuid
import os
import quantaAgrox as quantaAgrox
import RF_pwr_ctl
import energy_mesure
import SendWDT
import bacup_data_service

try:
    # Open the file for writing.
    os.system('pwd')
    os.chdir("rasppi_firmware_4g_lora") #rasppi_firmware_4g_lora
    os.system('pwd')
except:
    pass

### Init SysLed
import SysLed
SysLed.led()
SysLed.led.show(1,0x000000)
SysLed.led.show(2,0x000000)
SysLed.led.show(3,0x000000)
SysLed.led.show(4,0x000000)
# time.sleep(10)

############ Generate New UUID ########################
try:
    import Constants
    import Quanta_Env
    UUID = Quanta_Env.UUID
    LogService.LogService().info('\n\nUUID:{}'.format(UUID))
    
except Exception as e:
    LogService.LogService().info('Err:{}'.format(e))
    import Constants
    Constants.UUID = New_uuid = str(uuid.uuid4())
    New_uuid = 'UUID="'+ New_uuid + '"\n'
    New_uuid = bytes(New_uuid,'utf-8')
    LogService.LogService().info('Generate new uuid')
    file = open('Quanta_Env.py', "w+b")
    file.write(New_uuid)
    file.close()
    LogService.LogService().info('Generate sucess:{}\nReboot system'.format(New_uuid))
    time.sleep(3)
    os.system('reboot')
    pass

try:
    LogService.LogService().info('get cpuinfo')
    ls_cpuinfo = subprocess.run(['tail','/proc/cpuinfo'], capture_output=True) # get last 10 line in file
    Constants.CPUINFO = ls_cpuinfo.stdout
    file = open('cpuinfo.txt', "w+b")
    file.write(ls_cpuinfo.stdout)
    file.close()
    LogService.LogService().info('*** Create file cpuinfo')
    ################ write cpu model to Constants
    ls_cpuinfo = subprocess.run(['tail','-1','/proc/cpuinfo'], capture_output=True) # get last 10 line in file
    Constants.MODEL = ls_cpuinfo.stdout
except:
    pass

try:
    file = open('rf_config.py', "r")
except Exception as e:
    LogService.LogService().info('rf_config Err:{}'.format(e))
    os.system('cp rf_config.bac rf_config.py')
    os.system('cat rf_config.py')
    pass

name = os.uname()
MachineNodename = name.nodename
if 'raspberrypi' or 'Raspberypi' in MachineNodename :
    MachineNodename = 'pi'
    LogService.LogService().info('\n\nMachine hardware is Raspberrypi')
else:
    MachineNodename = 'x86'
    LogService.LogService().info('\n\nMachine hardware is Computer')

if MachineNodename == 'pi':
    ls_file = subprocess.run(['ls','-al'], capture_output=True)
    file = open('file_list.txt', "w+b")
    file.write(ls_file.stdout)
    file.close()
    
# try:
#     file = open('git_log.txt', "w+b")
#     if MachineNodename == 'pi':
#         LogService.LogService().info('Check update Firmware..')
#         retval = subprocess.run(['git','reset','--hard'], capture_output=True)
#         LogService.LogService().info(retval.stdout)
#         file.write(retval.stdout)
#         retval = subprocess.run(['git','pull'], capture_output=True)
#         LogService.LogService().info(retval.stdout)
#         file.write(retval.stdout)
#         retval = subprocess.run(['git','log','-n','1'], capture_output=True)
#         LogService.LogService().info(retval.stdout)
#         file.write(retval.stdout)
#         file.close()
# except Exception as err:
#     LogService.LogService().info('err:{}'.format(err))
#     pass
# time.sleep(10)

##### Reset Lora Module
LogService.LogService().info('Reset Lora Module')
RF_pwr_ctl.rf_set_state(False) ### Set GPIO5, Pi pin 29 to Logic High
time.sleep(0.25)
RF_pwr_ctl.rf_set_state(True) ### Set GPIO5, Pi pin 29 to Logic High

import MqttService as MqttService
import SendData as SendData

def led_blink():
    time.sleep(0.15)
    SysLed.led.show(1,0x000000)
    time.sleep(0.15)
    SysLed.led.show(1,0x001F00)
class main:
    def __init__(self):

        logging.info(Constants.SOFTWARE_TITLE)
        LogService.LogService().info(Constants.SOFTWARE_TITLE)

        ### Delay For Lora boot ready
        led_blink()
        led_blink()
        led_blink()

        SysLed.led.show(1,0xFFFFFF)
        quantaAgrox.Agrox()
        time.sleep(1)

        SysLed.led.show(2,0xFFFFFF)
        _thread.start_new_thread(MqttService.MqttService, ())
        time.sleep(1)

        SysLed.led.show(3,0xFFFFFF) 
        _thread.start_new_thread(SendData.SendDataService, ())
        time.sleep(1)

        SysLed.led.show(4,0xFFFFFF)
        _thread.start_new_thread(quantaAgrox.Agrox.agrox_run , ())

        _thread.start_new_thread(SendWDT.Send_data_wathdog, ())

        _thread.start_new_thread(bacup_data_service.BackupData, ())

        time.sleep(5)
        led_blink()
        led_blink()
        led_blink()
        energy_mesure.mesure()
        LogService.LogService().info('-Run energy_mesure')
        SysLed.led.show(1,0x001F00)
        SysLed.led.show(2,0x000000)
        SysLed.led.show(3,0x000000)      
        SysLed.led.show(4,0x000200)
        _thread.start_new_thread(energy_mesure.mesure.read(), ())

        LogService.LogService().info('Exit and Reboot')
        time.sleep(10)
        # os.system('reboot')
        while True:
            time.sleep(10)
        LogService.LogService().info('Exit Bootloader main')
        
        
if __name__ == '__main__':
    main()